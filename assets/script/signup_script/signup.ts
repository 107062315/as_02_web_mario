// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class signup extends cc.Component {


    signup()
    {
        var username=cc.find("Canvas/textarea/textarea_bg/username_bg/username");
        var username_txt=username.getComponent(cc.EditBox).string;
        var email=cc.find("Canvas/textarea/textarea_bg/email_bg/email");
        var email_txt=email.getComponent(cc.EditBox).string;
        var password=cc.find("Canvas/textarea/textarea_bg/password_bg/password");
        var password_txt=password.getComponent(cc.EditBox).string;
        firebase.auth().createUserWithEmailAndPassword(email_txt, password_txt).then(function()
        {
            firebase.database().ref('user/'+username_txt).set("");

            var mario_life=5;
            var score=0;
            firebase.database().ref('user/'+username_txt+"/life").set(mario_life);
            firebase.database().ref('user/'+username_txt+"/score").set(score);
            firebase.database().ref('user/'+username_txt+"/CanPlay").set(1);
        },function(error){alert(error);}).then(function()
        {
            firebase.auth().onAuthStateChanged(function(user)
            {
                if(user)
                {
                    user.updateProfile({
                        displayName:username_txt
                    }).then(function()
                    {
                        cc.director.loadScene("usermain");
                    })
                }
            })
        })
    }

    cancel()
    {
        cc.director.loadScene("begin");
    }
}
