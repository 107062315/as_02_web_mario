// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class player extends cc.Component {

    private first_become_small: boolean = true;

    private super_state: boolean = false;

    private no_move: boolean = false;

    sound_script;

    mario_die: boolean = false;

    private first_contact: boolean = true;

    private first_contact_turtle: boolean = true;

    //吃香菇變大
    isbig: boolean = false;

    //mario生命
    private life;

    //分數
    score;

    //時間
    private time: number = 300;

    //mario只過關一次
    private first_complete: boolean = true;

    //mario生命只能扣1次
    private first_die: boolean = true;

    //動畫
    private anim = null;


    //按鍵
    kDown: boolean = false;
    aDown: boolean = false;
    dDown: boolean = false;
    playerspeed: number;

    @property(cc.Node)
    camera: cc.Node = null;
    initial_cameraX: number;

    player_velocity: cc.Vec2;

    onLoad() {

        this.sound_script = cc.find("Canvas/audio").getComponent("audio");

        //得到UI資料
        var node = cc.director.getScene().getChildByName('global');
        this.life = node.getComponent('global').getdata().life;
        this.score = node.getComponent('global').getdata().score;

        this.initial_cameraX = this.node.x + 430;
        this.camera.x = this.initial_cameraX;
        this.anim = this.getComponent(cc.Animation);
        cc.director.getPhysicsManager().enabled = true;
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);

        //時間倒數
        cc.find("Canvas/UI/time_label").getComponent(cc.Label).string = ": " + this.time;
    }

    start() {
        cc.find("Canvas/UI/life_label").getComponent(cc.Label).string = ": " + this.life;
        cc.find("Canvas/UI/score_label").getComponent(cc.Label).string = "score: " + this.score;
        //時間倒數
        this.schedule(this.time_run, 1);
    }

    update(dt) {

        if (this.time == 0 && this.node.x >= cc.find("Canvas/flag").x + 50) {
            var mario_life = this.life;
            var score = this.score;
            firebase.auth().onAuthStateChanged(function (user) {
                if (user) {
                    firebase.database().ref("user/" + user.displayName).set(
                        {
                            life: mario_life,
                            score: score,
                            CanPlay: 2
                        }
                    ).then(function () {
                        cc.director.loadScene("usermain");
                    })

                }
            })
        }

        //摸旗
        if (this.node.x >= cc.find("Canvas/flag").x + 50 && this.first_complete) {
            this.no_move = true;
            this.first_complete = false;
            this.unschedule(this.time_run);
            var mario_life = this.life;
            cc.find("Canvas/score_bg").opacity = 255;
            cc.find("Canvas/complete").opacity = 255;
            cc.find("Canvas/score").opacity = 255;
            cc.find("Canvas/time").opacity = 255;
            this.schedule(function () {
                if (this.time > 0) {
                    this.time--;
                    this.score += 50;
                    cc.find("Canvas/time").getComponent(cc.Label).string = "TIME: " + this.time;
                    cc.find("Canvas/score").getComponent(cc.Label).string = "SCORE: " + this.score;
                }
            }, 0.01);
        }

        //分數
        cc.find("Canvas/UI/score_label").getComponent(cc.Label).string = "score: " + this.score;

        //時間倒數到0
        if (this.time == 0) this.unschedule(this.time_run);

        if (this.initial_cameraX <= this.node.x && this.node.x <= 3841) {
            this.camera.x = this.node.x;
            cc.find("Canvas/UI").x = this.camera.x - 374.815;
        }
        this.playermove(dt);
        //時間倒數
        cc.find("Canvas/UI/time_label").getComponent(cc.Label).string = ": " + this.time;

        //mario死掉後讓生命-1 && 換場景
        if (this.node.y <= -2000 && this.first_die) {
            this.first_die = false;

            //時間暫停
            this.unschedule(this.time_run);

            var mario_life = this.life;
            mario_life--;

            var score = this.score;

            firebase.auth().onAuthStateChanged(function (user) {
                if (user) {
                    if (mario_life == 0) {
                        mario_life = 5;
                        score = 0;
                        firebase.database().ref("user/" + user.displayName + "/life").set(mario_life).then(
                            function () {
                                firebase.database().ref("user/" + user.displayName + "/score").set(score);
                            }
                        ).then(function () {
                            cc.director.loadScene("game_over");
                        })
                    }
                    else {
                        firebase.database().ref("user/" + user.displayName + "/life").set(mario_life).then(
                            function () {
                                firebase.database().ref("user/" + user.displayName + "/score").set(score);
                            }
                        ).then(function () {
                            cc.director.loadScene("game_start");
                        })
                    }
                }
            })
        }
    }

    playermove(dt) {
        //mario死了
        if (this.mario_die == true) {
            this.anim.play("mario_die");
            return;
        }

        this.playerspeed = 0;

        if (this.aDown && !this.no_move) {
            this.playerspeed = -200;
            this.node.scaleX = -1;
            if (this.isbig) {
                var animState = this.anim.getAnimationState('mario_big_move');
                animState.speed = 2;
                if (!animState.isPlaying) this.anim.play("mario_big_move");
            }
            else {
                var animState = this.anim.getAnimationState('mario_move');
                animState.speed = 2;
                if (!animState.isPlaying) this.anim.play("mario_move");
            }
        }
        else if (this.dDown && !this.no_move) {
            this.playerspeed = 200;
            this.node.scaleX = 1;
            if (this.isbig) {
                var animState = this.anim.getAnimationState('mario_big_move');
                animState.speed = 2;
                if (!animState.isPlaying) this.anim.play("mario_big_move");
            }
            else {
                var animState = this.anim.getAnimationState('mario_move');
                animState.speed = 2;
                if (!animState.isPlaying) this.anim.play("mario_move");
            }
        }
        this.node.x += this.playerspeed * dt;

        //y的速度是0才能跳
        var player_vY = this.getComponent(cc.RigidBody).linearVelocity.y;
        if (this.kDown && player_vY == 0 && !this.no_move) this.jump();

        //跳躍動畫
        if (player_vY != 0) {
            if (this.isbig) this.anim.play("mario_big_jump");
            else this.anim.play("mario_jump");
        }
        else {
            if (this.isbig) {
                if (!this.anim.getAnimationState('mario_big_move').isPlaying) {
                    this.anim.play("mario_big_idle");
                }
            }
            else {
                if (!this.anim.getAnimationState('mario_move').isPlaying) {
                    this.anim.play("mario_idle");
                }
            }
        }
    }

    onKeyDown(event) {
        if (event.keyCode == cc.macro.KEY.k) // press key k to jump
            this.kDown = true;
        else if (event.keyCode == cc.macro.KEY.a) {
            this.aDown = true;
            this.dDown = false;
        }
        else if (event.keyCode == cc.macro.KEY.d) {
            this.dDown = true;
            this.aDown = false;
        }
    }

    onKeyUp(event) {
        if (event.keyCode == cc.macro.KEY.k)
            this.kDown = false;
        else if (event.keyCode == cc.macro.KEY.a)
            this.aDown = false;
        else if (event.keyCode == cc.macro.KEY.d)
            this.dDown = false;
    }

    onPreSolve(contact, self, other) {
        if (this.mario_die) {
            contact.disabled = true;
            return;
        }
        if (other.node.name == "enemy_turtle") {
            if (this.super_state) {
                contact.disabled = true;
                return;
            }
            else contact.disabled = false;

            if (!this.first_contact_turtle) return;
            this.first_contact_turtle = false;

            var worldManifold = contact.getWorldManifold();
            var normal = worldManifold.normal;
            if (normal.y <= -0.9) {
                this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 700);//彈起來
                cc.audioEngine.playEffect(this.sound_script.audioClips[7], false);
            }
            else {
                if (this.isbig) {
                    this.shine();
                    this.isbig = false;
                    this.no_move = true;
                    this.super_state = true;
                    cc.audioEngine.playEffect(this.sound_script.audioClips[0], false);
                    this.scheduleOnce(function () {
                        this.no_move = false;
                    }, 1)
                    this.scheduleOnce(function () {
                        this.super_state = false;
                    }, 2)
                }
                else {
                    this.mario_die = true;//撞到怪物死掉
                    this.no_move = true;
                    cc.audioEngine.stopMusic();
                    cc.audioEngine.playEffect(this.sound_script.audioClips[6], false);
                    this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 700);
                    this.unschedule(this.time_run);

                }
            }
        }
    }

    onBeginContact(contact, self, other) {
        if (this.mario_die) {
            contact.disabled = true;
            return;
        }
        if (other.node.name == 'enemy_goomba') {

            if (this.super_state) {
                contact.disabled = true;
                return;
            }
            else contact.disabled = false;

            if (!this.first_contact) return;
            this.first_contact = false;

            var worldManifold = contact.getWorldManifold();
            var normal = worldManifold.normal;
            if (normal.y <= -0.9) {
                this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 700);//彈起來
                cc.audioEngine.playEffect(this.sound_script.audioClips[7], false);
            }
            else {
                if (this.isbig) {
                    this.shine();
                    this.isbig = false;
                    this.no_move = true;
                    this.super_state = true;
                    cc.audioEngine.playEffect(this.sound_script.audioClips[0], false);
                    this.scheduleOnce(function () {
                        this.no_move = false;
                    }, 1)
                    this.scheduleOnce(function () {
                        this.super_state = false;
                    }, 2)
                }
                else {
                    this.mario_die = true;//撞到怪物死掉
                    this.no_move = true;
                    cc.audioEngine.stopMusic();
                    cc.audioEngine.playEffect(this.sound_script.audioClips[6], false);
                    this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 700);
                    this.unschedule(this.time_run);

                }
            }
        }
        if (other.node.name == "mushroom") {
            cc.audioEngine.playEffect(this.sound_script.audioClips[1], false);
            contact.disabled = true;
            this.isbig = true;
            other.node.destroy();
        }
    }

    onEndContact(contact, self, other) {
        if (other.node.name == "enemy_goomba") {
            this.first_contact = true;
        }
        if (other.node.name == "enemy_turtle") {
            this.first_contact_turtle = true;
        }
    }


    jump() {
        cc.audioEngine.playEffect(this.sound_script.audioClips[4], false);
        this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 700);
    }

    time_run() {
        this.time--;
    }

    shine() {
        this.scheduleOnce(function () {
            this.node.opacity = 120;
        }, 0.2)
        this.scheduleOnce(function () {
            this.node.opacity = 255;
        }, 0.4)
        this.scheduleOnce(function () {
            this.node.opacity = 120;
        }, 0.6)
        this.scheduleOnce(function () {
            this.node.opacity = 255;
        }, 0.8)
    }
}
