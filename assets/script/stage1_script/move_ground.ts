// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class move_ground extends cc.Component {

    private speedX:number;
    private iniX:number;
    private dir:number;

    onLoad()
    {
        this.iniX=this.node.x;
        if(this.node.name=="move_ground")this.speedX=50;
        else this.speedX=-50;
    }

    update (dt) {
        this.node.x+=this.speedX*dt;
        if(this.node.name=="move_ground")
        {
            if(this.node.x>=this.iniX+125)this.speedX=-50;
            else if(this.node.x<=this.iniX)this.speedX=50;
        }
        else
        {
            if(this.node.x<=this.iniX-125)this.speedX=50;
            else if(this.node.x>=this.iniX)this.speedX=-50;
        }
    }
}
