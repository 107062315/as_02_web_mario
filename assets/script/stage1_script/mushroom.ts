// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class mushroom extends cc.Component {

    ini_mushroom_y:number;
    mushroom_can_move:boolean=false;
    speedX:number=0;
    eat_mushroom:boolean=false;

    onLoad ()
    {
        this.node.getComponent(cc.PhysicsBoxCollider).enabled=false;
        this.ini_mushroom_y=this.node.y;
    }

    start ()
    {

    }

    update (dt)
    {
        if(this.mushroom_can_move)
        {
            this.node.x+=this.speedX*dt;
        }


        
        //香菇上移完
        if(this.node.y>=this.ini_mushroom_y+38)
        {
            this.scheduleOnce(function()
            {
                this.node.getComponent(cc.PhysicsBoxCollider).enabled=true;
                this.node.getComponent(cc.RigidBody).gravityScale=10;
                this.mushroom_can_move=true;
                this.speedX=-100;
            },0.01)
        }

        //香菇往上移
        if(this.node.parent.getChildByName("question_box_mushroom").getComponent("question_box_mushroom").mushroom_out)
        {
            if(this.node.y<this.ini_mushroom_y+38)this.node.y+=1;
        }
    }

}
