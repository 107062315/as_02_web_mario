// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class gamestart extends cc.Component {

    start()
    {
        var choose;
        firebase.auth().onAuthStateChanged(function(user)
        {
            if(user)
            {
                firebase.database().ref("user/"+user.displayName).once('value').then(function(snapshot)
                {
                    //選擇第幾關
                    choose=snapshot.val().choose;

                    //設定UI資料
                    var node = cc.find('global').getComponent('global');
                    node.setdata(
                        {
                            life:snapshot.val().life,
                            score:snapshot.val().score
                        }
                    );
                }).then(function()
                {
                    if(choose==1)
                    cc.director.loadScene("stage1");
                    else
                    cc.director.loadScene("stage2");
                })
            }
        })
    }
}
