// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class usermain extends cc.Component {

    private vol;

    onLoad()
    {
        this.vol=cc.find("Canvas/sound");

        var username=cc.find("Canvas/UI/username");
        firebase.auth().onAuthStateChanged(function(user)
        {
            if(user)
            {
                username.getComponent(cc.Label).string=user.displayName;
                firebase.database().ref("user/"+user.displayName).once('value').then(function(snapshot)
                {
                    cc.find("Canvas/UI/life_label").getComponent(cc.Label).string=": "+snapshot.val().life;
                    cc.find("Canvas/UI/score_label").getComponent(cc.Label).string="score: "+snapshot.val().score;
                    if(snapshot.val().CanPlay==1)
                    {
                        cc.find("Canvas/stage2").opacity=100;
                        cc.find("Canvas/stage2").getComponent(cc.Button).interactable=false;
                    }
                    else 
                    {
                        cc.find("Canvas/stage2").opacity=255;
                        cc.find("Canvas/stage2").getComponent(cc.Button).interactable=true;
                    }
                })
            }
        })
    }

    update()
    {
        var vol=this.vol.getComponent(cc.Slider).progress;
        cc.audioEngine.setMusicVolume(vol);
    }

    stage1()
    {
        firebase.auth().onAuthStateChanged(function(user)
        {
            if(user)
            {
                firebase.database().ref("user/"+user.displayName+"/choose").set(1).then(function()
                {
                    cc.director.loadScene("game_start");
                })
            }
        })
    }

    stage2()
    {
        firebase.auth().onAuthStateChanged(function(user)
        {
            if(user)
            {
                firebase.database().ref("user/"+user.displayName+"/choose").set(2).then(function()
                {
                    cc.director.loadScene("game_start");
                })
            }
        })
    }
}
