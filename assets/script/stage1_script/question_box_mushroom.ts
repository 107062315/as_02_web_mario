// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class question_box_mushroom extends cc.Component {

    private anim=null;

    box_off:boolean=false;
    mushroom_out:boolean=false;

    sound_script;

    onLoad()
    {
        this.sound_script=cc.find("Canvas/audio").getComponent("audio");
        this.anim=this.node.getComponent(cc.Animation);
    }

    start()
    {
        this.anim.play("question_box_move");
    }


    onBeginContact(contact, self, other)
    {
        if(other.node.name=='player')
        {
            var worldManifold = contact.getWorldManifold ();
            var normal = worldManifold.normal;
            if(normal.y==-1)
            {
                if(!this.box_off)
                {
                    cc.audioEngine.playEffect(this.sound_script.audioClips[2],false);
                    cc.find("Canvas/player").getComponent("player").score+=100;
                    this.mushroom_out=true;
                }
                this.anim.stop();
                this.anim.play("question_box_off");
                this.box_off=true;    
            }
        }
    }
}
