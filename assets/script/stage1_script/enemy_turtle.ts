// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class enemy_turtle extends cc.Component {

    sound_script;
    private first_sound:boolean=true;

    private anim = null;
    private die:boolean;
    private animateState=null;
    private make_move:boolean=false;
    private move_stop:boolean=false;
    private move_fast:boolean=false;
    private first_contact:boolean=true;

    private speedX=-90;
    private speedX_plus=-200;

    onLoad()
    {
        this.sound_script = cc.find("Canvas/audio").getComponent("audio");
        this.die=false;
        this.anim = this.node.getComponent(cc.Animation);
    }

    start()
    {
        this.anim.play("enemy_turtle_walk");
    }

    update(dt)
    {
        if(!this.move_stop&&!cc.find("Canvas/player").getComponent("player").no_move)
        {
            if(this.move_fast)this.node.x+=this.speedX_plus*dt;
            else this.node.x+=this.speedX*dt;
        }
        if(this.node.y<=-400)
        {
            this.node.destroy();
        }
    }

    onPreSolve(contact, self, other)
    {
        var worldManifold = contact.getWorldManifold ();
        var normal = worldManifold.normal;

        if(other.node.name=="ground")
        {
            if(this.die)contact.disabled=true;
        }

        if (other.node.name == 'player')
        {
            if(other.node.getComponent("player").mario_die||other.node.getComponent("player").super_state)return;

            if(!this.first_contact)return;
            this.first_contact=false;

            if(normal.y>0.5)
            {
                this.anim.stop();
                if(this.make_move)
                {
                    this.move_stop=false;
                    this.anim.play("enemy_turtle_shell_move");
                    this.move_fast=true;
                }
                else
                {
                    this.anim.play("enemy_turtle_shell");
                    this.move_stop=true;
                    this.move_fast=false;
                }
                if(this.make_move==false)this.make_move=true;
                else this.make_move=false;
            }            
        }

        if(other.node.name=="block")
        {
            this.speedX*=-1;
            this.speedX_plus*=-1;
        }

        if(other.node.name=="enemy_turtle")
        {
            contact.disabled=true;
            if(other.node.getComponent("enemy_turtle").move_fast)
            {
                this.die=true;
                this.move_stop=true;
                if(this.first_sound)
                {
                    cc.audioEngine.playEffect(this.sound_script.audioClips[5], false);
                    this.first_sound=false;
                }
            }
        }
    }


    onEndContact(contact, self, other)
    {
        if(other.node.name=="block")
        {
            this.node.scaleX*=-1;
        }
        if(other.node.name=="player")
        {
            this.first_contact=true;
        }
    }
}
