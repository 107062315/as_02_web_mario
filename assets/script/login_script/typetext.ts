// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class typetext extends cc.Component {

    //登入按鈕
    login()
    {
        var email=cc.find("Canvas/textarea/textarea_bg/email_bg/email");
        var email_txt=email.getComponent(cc.EditBox).string;
        var password=cc.find("Canvas/textarea/textarea_bg/password_bg/password");
        var password_txt=password.getComponent(cc.EditBox).string;
        firebase.auth().signInWithEmailAndPassword(email_txt, password_txt).then(function()
        {
            cc.director.loadScene("usermain");
        },function(error)
        {
            alert(error);
        })
    }
   
    cancel()
    {
        cc.director.loadScene("begin");
    }
    
}
