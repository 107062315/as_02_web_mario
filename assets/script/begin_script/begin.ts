// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class begin extends cc.Component {

    sound_script;
    private vol;

    onLoad()
    {
        this.vol=cc.find("Canvas/sound");
        this.sound_script = cc.find("Canvas/audio").getComponent("audio");
    }

    start()
    {
        if(!cc.audioEngine.isMusicPlaying())cc.audioEngine.playMusic(this.sound_script.audioClips[0], true);
    }

    update()
    {
        var vol=this.vol.getComponent(cc.Slider).progress;
        cc.audioEngine.setMusicVolume(vol);
    }

    loginScene()
    {
        cc.director.loadScene("login");
    }
    siunupScene()
    {
        cc.director.loadScene("signup");
    }

    
}
