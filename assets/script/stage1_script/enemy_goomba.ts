// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class enemy_goomba extends cc.Component {

    sound_script;
    private first_sound:boolean=true;

    private anim = null;
    private die:boolean=false;
    private move_stop:boolean=false;
    private first_contact:boolean=true;

    private speedX:number=-100;

    enemy_walk() {
        this.node.scaleX *= -1;
    }

    onLoad() {
        this.sound_script = cc.find("Canvas/audio").getComponent("audio");
        this.anim = this.node.getComponent(cc.Animation);
        this.schedule(this.enemy_walk, 0.2);
    }


    update(dt)
    {
        if(!this.move_stop&&!cc.find("Canvas/player").getComponent("player").no_move)this.node.x+=this.speedX*dt;
        if(this.node.y<=-400)
        {
            this.node.destroy();
        }
    }

    onBeginContact(contact, self, other) {
        var worldManifold = contact.getWorldManifold ();
        var normal = worldManifold.normal;

        if(other.node.name=="ground")
        {
            if(this.die)contact.disabled=true;
        }

        if (other.node.name == 'player')
        {
            if(other.node.getComponent("player").mario_die||other.node.getComponent("player").super_state)return;

            if(!this.first_contact)return;
            this.first_contact=false;

            if(normal.y>=0.9)
            {
                this.move_stop=true;
                this.anim.play("enemy_goomba_die");
                this.scheduleOnce(function () {
                    this.node.destroy();
                }, 0.3);
            }
        }

        if(other.node.name=="block")
        {
            this.speedX*=-1;
        }

        if(other.node.name=="enemy_turtle")
        {
            contact.disabled=true;
            var turtle_shell= other.node.getComponent(cc.Animation).getAnimationState('enemy_turtle_shell_move');
            if(turtle_shell.isPlaying)
            {
                this.die=true;
                this.move_stop=true;
                if(this.first_sound)
                {
                    cc.audioEngine.playEffect(this.sound_script.audioClips[5], false);
                    this.first_sound=false;
                }
            }
        }

    }

    onEndContact(contact, self, other)
    {
        if(other.node.name=="ground")
        {
            if(this.die)contact.disabled=true;
        }
        if(other.node.name=="player")
        {
            this.first_contact=true;
        }
    }
}
