// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html


const {ccclass, property} = cc._decorator;

@ccclass
export default class question_box extends cc.Component {

    private anim=null;

    box_off:boolean=false;

    sound_script;

    onLoad()
    {
        this.sound_script=cc.find("Canvas/audio").getComponent("audio");
        this.anim=this.node.getComponent(cc.Animation);
    }

    start()
    {
        this.anim.play("question_box_move");
    }

    onBeginContact(contact, self, other)
    {
        if(cc.find("Canvas/player").getComponent("player").mario_die)return;
        if(other.node.name=='player')
        {
           var worldManifold = contact.getWorldManifold ();
           var normal = worldManifold.normal;
           if(normal.y==-1)
           {
                if(!this.box_off)
                {
                    cc.audioEngine.playEffect(this.sound_script.audioClips[3],false);
                    cc.find("Canvas/player").getComponent("player").score+=100;
                    self.node.getChildByName("money").opacity=255;
                    this.scheduleOnce(function()
                    {
                        self.node.getChildByName("money").destroy();
                    },0.5)
                    self.node.getChildByName("money").getComponent(cc.Animation).play();
                }
                this.anim.stop();
                this.anim.play("question_box_off");
                this.box_off=true;
           }
        }
    }
}
